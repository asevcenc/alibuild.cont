# alibuild.cont

Containers for ALICE software building

Docker format containers:
* gitlab-registry.cern.ch/asevcenc/alibuild.cont/alibuild_el7:latest
* gitlab-registry.cern.ch/asevcenc/alibuild.cont/alibuild_el8:latest

Singularity containers:
* oras://registry.cern.ch/asevcenc/alibuild_el7:latest
* oras://registry.cern.ch/asevcenc/alibuild_el8:latest


